 <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
 <%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fm" %>
 
<div class="container" style="margin-bottom: 10px ; width: 600px" >
		<h1>Gestion des produits :</h1>
		    <c:if test="${not empty requestScope.error}">
			  <div class="alert alert-danger">
			  <strong>Danger!</strong> ${requestScope.error}
			</div>
			</c:if>
		<form action='${pageContext.request.contextPath}/Produit' method="post">
	  <div class="form-group">
	    <label for="1">ref : </label>
	    <input name="ref" type="text" class="form-control" id="1" placeholder="M16dfsc" required="required"  value="${requestScope.produit._ref}">
	  </div>
	  <div class="form-group">
	    <label for="2">nom : </label>
	    <input name="nom" type="text" class="form-control" id="2" placeholder="laptop" required="required" value="${requestScope.produit._nom}">
	  </div>
	  <div class="form-group">
	    <label for="3">modele : </label>
	    <input name="modele" type="text" class="form-control" id="3" placeholder="ideapad 320s" required="required" value="${requestScope.produit._modele}">
	  </div>
	  <div class="form-group">
	    <label for="4">date production : </label>
	    <input name="date_production" type="date" min="1970-01-01" max="3000-12-31" class="form-control" id="4" placeholder="2016-02-15" required="required" 
	    value="<fm:formatDate pattern = 'yyyy-MM-dd' value = '${requestScope.produit._date_production}'/>">
	  </div>
	  <div class="form-group">
	    <label for="4">quantite : </label>
	    
	    <input name="quantite" type="number" min="0" max="9999" class="form-control" id="4" placeholder="5" required="required" value="${requestScope.produit._quantite}">
	  </div>
	  
	  <input name="id" type="hidden" value="${requestScope.produit._iD}">
	  
	  <input name="add" type="submit" class="btn"  required="required">
	  
	</form>
</div>
