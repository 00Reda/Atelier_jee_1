 <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="container" style="margin-bottom: 10px ; width: 600px" >
		<h1>Gestion des clients :</h1>
		    <c:if test="${not empty requestScope.error}">
			  <div class="alert alert-danger">
			  <strong>Danger!</strong> ${requestScope.error}
			</div>
			</c:if>
		<form action='${pageContext.request.contextPath}/Client' method="post">
	  <div class="form-group">
	    <label for="1">Email : </label>
	    <input name="email" type="email" class="form-control" id="1" placeholder="name@example.com" required="required"  value="${requestScope.client._email}">
	  </div>
	  <div class="form-group">
	    <label for="2">nom : </label>
	    <input name="nom" type="text" class="form-control" id="2" placeholder="rida htiti" required="required" value="${requestScope.client._nom}">
	  </div>
	  <div class="form-group">
	    <label for="3">tel : </label>
	    <input name="tel" type="text" class="form-control" id="3" placeholder="+2126565656" required="required" value="${requestScope.client._tel}">
	  </div>
	  <div class="form-group">
	    <label for="4">password : </label>
	    <input name="password" type="password" class="form-control" id="4" placeholder="*******" required="required" value="${requestScope.client._password}">
	  </div>
	  
	  <input name="id" type="hidden" value="${requestScope.client._iD}">
	  
	  <input name="add" type="submit" class="btn"  required="required">
	  
	</form>
</div>
