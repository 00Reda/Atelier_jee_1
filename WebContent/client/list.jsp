<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"
    %>
<jsp:include page="../include/header.jsp"></jsp:include>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<body>

 <div class="container">
 
    <h1>list des clients !</h1>
    <div class="container">
 		
	    <!-- Button trigger modal -->
		<a href="${pageContext.request.contextPath}/Client" class="btn btn-primary" >
		  Ajouter
		</a>   
		
    </div>
    
    <c:if test="${not empty requestScope.error}">
	  <div class="alert alert-danger">
	  <strong>Danger!</strong><c:out value=" ${requestScope.error}"></c:out>
	</div>
	</c:if>
	
	<c:if test="${not empty requestScope.msg}">
	  <div class="alert alert-primary">
	  <strong>info!</strong><c:out value=" ${requestScope.msg}"></c:out>
	</div>
	</c:if>
	
	 <c:if test="${not empty sessionScope.error}">
	  <div class="alert alert-danger">
	  <strong>Danger!</strong><c:out value=" ${sessionScope.error}"></c:out>
	  <c:remove var="error" scope="session"/>
	</div>
	</c:if>
	
	<c:if test="${not empty sessionScope.msg}">
	  <div class="alert alert-primary">
	  <strong>info!</strong><c:out value=" ${sessionScope.msg}"></c:out>
	  <c:remove var="msg" scope="session"/>
	</div>
	</c:if>
      
    <c:if test="${ fn:length(requestScope.list) > 0}">
	    <table class="table " style="margin:7px">
	  <thead>
	    <tr>
	      <th scope="col">#</th>
	      <th scope="col">email</th>
	      <th scope="col">nom</th>
	      <th scope="col">token</th>
	      <th scope="col">tel</th>
	      <th scope="col">creation</th>
	      <th scope="col">derniere connexion</th>
	      <th scope="col">Action</th>
	      <th scope="col">Commandes</th>
	      
	      
	    </tr>
	  </thead>
	  <tbody>
	  <c:forEach items="${requestScope.list}" var="client">
	    <tr>
	      <th scope="row">${client._iD}</th>
	      <td>${client._email}</td>
	      <td>${client._nom}</td>
	      <td>${client._token}</td>
	      <td>${client._tel}</td>
	      <td>${client._created_at}</td>
	      <td>${client._last_login}</td>
	      <td>
	           <a href="${pageContext.request.contextPath}/Client?id=${client._iD}" class="btn btn-info" role="button">Modifier</a>
	           <a href="${pageContext.request.contextPath}/Client/delete?id=${client._iD}" onclick="return confirm('vous etes sure de vouloir supprimer :')" class="btn btn-info" role="button">SUPP</a>
	      </td>
	      <td>
	           <a href="${pageContext.request.contextPath}/Commande/list?id=${client._iD}" class="btn btn-info" role="button">voir</a>
	           <a href="${pageContext.request.contextPath}/Commande/?id=${client._iD}" class="btn btn-info" role="button">Add</a>
	           
	      </td>
	      
	    </tr>
	    </c:forEach>
	  </tbody>
	</table>
	</c:if>
	
	
</div>
		


<jsp:include page="../include/footer.jsp"></jsp:include>
</body>
</html>