<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"
    %>
<jsp:include page="../include/header.jsp"></jsp:include>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<body>

 <div class="container">
 
    <h1>list des produits de la commande : </h1>
    <c:set var="commande" value="${requestScope.commande}"></c:set>
    <table class="table " style="margin:7px">
	  <thead>
	    <tr>
	      <th scope="col">#</th>
	      <th scope="col">date</th>
	      <th scope="col">client</th>
	      <th scope="col">produits</th>
	    </tr>
	  </thead>
	  <tbody>
	    <tr>
	      <th scope="row">${commande._iD}</th>
	      <td>${commande._date}</td>
	      <td>
	           <a href="${pageContext.request.contextPath}/Client/list?id=${commande._iD}" class="btn btn-info" role="button">voir</a>
	      </td>
	      <td>
	           <a href="${pageContext.request.contextPath}/ligneCommande/list?id=${commande._iD}" class="btn btn-info" role="button">voir</a>
	      </td>
	     
	    </tr>
	  </tbody>
	</table>
    <hr><hr><hr>
    <div class="container">
 		
	    <!-- Button trigger modal -->
		<a href="#" class="btn btn-primary" >
		  Ajouter
		</a>   
		
    </div>
    
    <c:if test="${not empty requestScope.error}">
	  <div class="alert alert-danger">
	  <strong>Danger!</strong><c:out value=" ${requestScope.error}"></c:out>
	</div>
	</c:if>
	
	<c:if test="${not empty requestScope.msg}">
	  <div class="alert alert-primary">
	  <strong>info!</strong><c:out value=" ${requestScope.msg}"></c:out>
	</div>
	</c:if>
	
	 <c:if test="${not empty sessionScope.error}">
	  <div class="alert alert-danger">
	  <strong>Danger!</strong><c:out value=" ${sessionScope.error}"></c:out>
	  <c:remove var="error" scope="session"/>
	</div>
	</c:if>
	
	<c:if test="${not empty sessionScope.msg}">
	  <div class="alert alert-primary">
	  <strong>info!</strong><c:out value=" ${sessionScope.msg}"></c:out>
	  <c:remove var="msg" scope="session"/>
	</div>
	</c:if>
      
    <c:if test="${ fn:length(requestScope.list) > 0}">
	    <table class="table " style="margin:7px">
	  <thead>
	    <tr>
	      <th scope="col">#</th>
	      <th scope="col">produit</th>
	      <th scope="col">Commande</th>
	      <th scope="col">quantite</th>
	      <th scope="col">Action</th>
	     
	      
	      
	    </tr>
	  </thead>
	  <tbody>
	  <c:forEach items="${requestScope.list}" var="client">
	    <tr>
	      <th scope="row">${client._iD}</th>
		      <td>
	           <a href="${pageContext.request.contextPath}/Produit/list?id=${client._iD}" class="btn btn-info" role="button">voir</a>
	      </td>
	      <td>
	           <a href="${pageContext.request.contextPath}/Commande/list?id=${client._iD}" class="btn btn-info" role="button">voir</a>
	      </td>
	      <th scope="row">${client._quntite}</th>
	      <td>
	           <a href="#" class="btn btn-info" role="button">Modifier</a>
	           <a href="#" onclick="return confirm('vous etes sure de vouloir supprimer :')" class="btn btn-info" role="button">SUPP</a>
	      </td>
	    
	      
	    </tr>
	    </c:forEach>
	  </tbody>
	</table>
	</c:if>
	
	
</div>
		


<jsp:include page="../include/footer.jsp"></jsp:include>
</body>
</html>