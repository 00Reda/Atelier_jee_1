<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"
    %>
<jsp:include page="../include/header.jsp"></jsp:include>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<body>

 <div class="container">
 
    <h1>list des produits !</h1>
    <div class="container">
 		
	    <!-- Button trigger modal -->
		<a href="${pageContext.request.contextPath}/Produit" class="btn btn-primary" >
		  Ajouter
		</a>   
		
    </div>
    
    <c:if test="${not empty requestScope.error}">
	  <div class="alert alert-danger">
	  <strong>Danger!</strong><c:out value=" ${requestScope.error}"></c:out>
	</div>
	</c:if>
	
	<c:if test="${not empty requestScope.msg}">
	  <div class="alert alert-primary">
	  <strong>info!</strong><c:out value=" ${requestScope.msg}"></c:out>
	</div>
	</c:if>
	
	 <c:if test="${not empty sessionScope.error}">
	  <div class="alert alert-danger">
	  <strong>Danger!</strong><c:out value=" ${sessionScope.error}"></c:out>
	  <c:remove var="error" scope="session"/>
	</div>
	</c:if>
	
	<c:if test="${not empty sessionScope.msg}">
	  <div class="alert alert-primary">
	  <strong>info!</strong><c:out value=" ${sessionScope.msg}"></c:out>
	  <c:remove var="msg" scope="session"/>
	</div>
	</c:if>
      
    <c:if test="${ fn:length(requestScope.list) > 0}">
	    <table class="table " style="margin:7px">
	  <thead>
	    <tr>
	      <th scope="col">#</th>
	      <th scope="col">ref</th>
	      <th scope="col">nom</th>
	      <th scope="col">modele</th>
	      <th scope="col">date production</th>
	      <th scope="col">qantete</th>
	      <th scope="col">Action</th>
	    </tr>
	  </thead>
	  <tbody>
	  <c:forEach items="${requestScope.list}" var="client">
	    <tr id="${client._iD}">
	      <th scope="row">${client._iD}</th>
	      <td>${client._ref}</td>
	      <td>${client._nom}</td>
	      <td>${client._modele}</td>
	      <td>${client._date_production}</td>
	      <td>${client._quantite}</td>
	      <td>
	           <a href="${pageContext.request.contextPath}/Produit?id=${client._iD}" class="btn btn-info" role="button">Modifier</a>
	           <a href="${pageContext.request.contextPath}/Produit/delete?id=${client._iD}" onclick="return confirm('vous etes sure de vouloir supprimer :')" id="_delete" class="btn btn-info" role="button">SUPP</a>
	      </td>
	    </tr>
	    </c:forEach>
	  </tbody>
	</table>
	</c:if>
	
	
</div>
		


<jsp:include page="../include/footer.jsp"></jsp:include>
</body>
</html>