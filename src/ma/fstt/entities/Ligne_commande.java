package ma.fstt.entities;

public class Ligne_commande {
	
	private int _iD;
	private int _quntite;
	private Commande _commande;
	private Produit _produit;
	
	
	public Ligne_commande() {
		super();
	}

	public Ligne_commande(int _iD, int _quntite, Commande _commande, Produit _produit) {
		super();
		this._iD = _iD;
		this._quntite = _quntite;
		this._commande = _commande;
		this._produit = _produit;
	}
	
	public int get_iD() {
		return _iD;
	}
	public void set_iD(int _iD) {
		this._iD = _iD;
	}
	public int get_quntite() {
		return _quntite;
	}
	public void set_quntite(int _quntite) {
		this._quntite = _quntite;
	}
	public Commande get_commande() {
		return _commande;
	}
	public void set_commande(Commande _commande) {
		this._commande = _commande;
	}
	public Produit get_produit() {
		return _produit;
	}
	public void set_produit(Produit _produit) {
		this._produit = _produit;
	}

	
}