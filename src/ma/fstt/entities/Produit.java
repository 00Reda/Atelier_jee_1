package ma.fstt.entities;

import java.util.Vector;

import java.util.Date;

public class Produit {
	
	private int _iD;
	private String _ref;
	private String _nom;
	private String _modele;
	private Date _date_production;
	private int _quantite;
	private Vector<Ligne_commande> _detail_vend = new Vector<Ligne_commande>();
	
	
	
	public Produit() {
		super();
	}


	public Produit(int _iD, String _ref, String _nom, String _modele, Date _date_production, int _quantite,
			Vector<Ligne_commande> _detail_vend) {
		super();
		this._iD = _iD;
		this._ref = _ref;
		this._nom = _nom;
		this._modele = _modele;
		this._date_production = _date_production;
		this._quantite = _quantite;
		this._detail_vend = _detail_vend;
	}
	
	
	public int get_iD() {
		return _iD;
	}
	public void set_iD(int _iD) {
		this._iD = _iD;
	}
	public String get_ref() {
		return _ref;
	}
	public void set_ref(String _ref) {
		this._ref = _ref;
	}
	public String get_nom() {
		return _nom;
	}
	public void set_nom(String _nom) {
		this._nom = _nom;
	}
	public String get_modele() {
		return _modele;
	}
	public void set_modele(String _modele) {
		this._modele = _modele;
	}
	public Date get_date_production() {
		return _date_production;
	}
	public void set_date_production(Date _date_production) {
		this._date_production = _date_production;
	}
	public int get_quantite() {
		return _quantite;
	}
	public void set_quantite(int _quantite) {
		this._quantite = _quantite;
	}
	public Vector<Ligne_commande> get_detail_vend() {
		return _detail_vend;
	}
	public void set_detail_vend(Vector<Ligne_commande> _detail_vend) {
		this._detail_vend = _detail_vend;
	}
	
	
	
}