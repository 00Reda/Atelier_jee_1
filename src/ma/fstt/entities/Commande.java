package ma.fstt.entities;

import java.util.Vector;

import java.util.Date;

public class Commande {
	
	private int _iD;
	private Date _date;
	private Client _client;
	private Vector<Ligne_commande> _detail_commande = new Vector<Ligne_commande>();

	
	
	
	public Commande() {
		super();
	}
	
	public Commande(int _iD, Date _date, Client _client, Vector<Ligne_commande> _detail_commande) {
		super();
		this._iD = _iD;
		this._date = _date;
		this._client = _client;
		this._detail_commande = _detail_commande;
	}
	
	public int get_iD() {
		return _iD;
	}
	public void set_iD(int _iD) {
		this._iD = _iD;
	}
	public Date get_date() {
		return _date;
	}
	public void set_date(Date _date) {
		this._date = _date;
	}
	public Client get_client() {
		return _client;
	}
	public void set_client(Client _client) {
		this._client = _client;
	}
	public Vector<Ligne_commande> get_detail_commande() {
		return _detail_commande;
	}
	public void set_detail_commande(Vector<Ligne_commande> _detail_commande) {
		this._detail_commande = _detail_commande;
	}
	
	
}