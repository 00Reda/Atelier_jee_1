package ma.fstt.entities;


import java.util.Vector;

import java.util.Date;;

public class Client implements Cloneable{
	
	private int _iD;
	private String _email;
	private String _nom;
	private String _password;
	private String _token;
	private String _tel;
	private Date _created_at;
	private Date _last_login;
	private Vector<Commande> _commandes = new Vector<Commande>();
	
	
	
	public Client() {
		super();
	}



	public Client(int _iD, String _email, String _nom, String _passwoed, String _token, String _tel, Date _created_at,
			Date _last_login, Vector<Commande> _commandes) {
		super();
		this._iD = _iD;
		this._email = _email;
		this._nom = _nom;
		this._password = _passwoed;
		this._token = _token;
		this._tel = _tel;
		this._created_at = _created_at;
		this._last_login = _last_login;
		this._commandes = _commandes;
	}



	public int get_iD() {
		return _iD;
	}



	public void set_iD(int _iD) {
		this._iD = _iD;
	}



	public String get_email() {
		return _email;
	}



	public void set_email(String _email) {
		this._email = _email;
	}



	public String get_nom() {
		return _nom;
	}



	public void set_nom(String _nom) {
		this._nom = _nom;
	}



	public String get_password() {
		return _password;
	}



	public void set_password(String _passwoed) {
		this._password = _passwoed;
	}



	public String get_token() {
		return _token;
	}



	public void set_token(String _token) {
		this._token = _token;
	}



	public String get_tel() {
		return _tel;
	}



	public void set_tel(String _tel) {
		this._tel = _tel;
	}



	public Date get_created_at() {
		return _created_at;
	}



	public void set_created_at(Date _created_at) {
		this._created_at = _created_at;
	}



	public Date get_last_login() {
		return _last_login;
	}



	public void set_last_login(Date _last_login) {
		this._last_login = _last_login;
	}



	public Vector<Commande> get_commandes() {
		return _commandes;
	}



	public void set_commandes(Vector<Commande> _commandes) {
		this._commandes = _commandes;
	}
	
	
	
	
}