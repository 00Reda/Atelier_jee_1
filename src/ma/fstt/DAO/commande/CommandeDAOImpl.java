package ma.fstt.DAO.commande;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import javax.enterprise.context.ApplicationScoped;

import ma.fstt.DAO.BaseDAOImpl;
import ma.fstt.entities.Client;
import ma.fstt.entities.Commande;
import ma.fstt.entities.Ligne_commande;
import ma.fstt.entities.Produit;

@ApplicationScoped
public class CommandeDAOImpl extends BaseDAOImpl implements ICommandeDAO{

	public CommandeDAOImpl() throws Exception {
		super();
		// TODO Auto-generated constructor stub
	}

	private Commande getObjectFromDB() throws Exception{
		
		 int _iD;
		 Date _date;
		 Client _client=new Client();
		 Vector<Ligne_commande> _detail_commande=new Vector<>();
				 
		 _iD=this.rs.getInt("ID");
		 _date=this.rs.getDate("date");
		 _client.set_iD(this.rs.getInt("clientID"));
		
		
		 Commande c=new Commande(_iD, _date, _client, _detail_commande);
		
		 return c;
	}
	
	@Override
	public Commande findByID(int ID) throws Exception {
		
        String sql ="select * from commande where ID='"+ID+"';";
		
		this.rs=this.stm.executeQuery(sql);
		
		if (this.rs.next()) {
			
			 Commande c=this.getObjectFromDB();
			 
			 return c;
			
		}
		return null;
	}

	@Override
	public List<Commande> list() throws Exception {
		// TODO Auto-generated method stub
		String sql ="select * from commande order by ID desc;";
		
		this.rs=this.stm.executeQuery(sql);
		ArrayList<Commande> list =new ArrayList<Commande>();
		while (this.rs.next()) {
			
			Commande c=this.getObjectFromDB();
			list.add(c);
			
		}
		
		return list;
	}
	
	public List<Commande> ListByClient(int clientID) throws Exception {
		// TODO Auto-generated method stub
		String sql ="select * from commande where clientID='"+clientID+"' order by ID desc;";
		
		this.rs=this.stm.executeQuery(sql);
		ArrayList<Commande> list =new ArrayList<Commande>();
		while (this.rs.next()) {
			
			Commande c=this.getObjectFromDB();
			list.add(c);
			
		}
		
		return list;
	}

	@Override
	public void add(Commande elm) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(Commande elm) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(int ID) throws Exception {
		// TODO Auto-generated method stub
		
	}

}
