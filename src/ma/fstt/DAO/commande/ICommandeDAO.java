package ma.fstt.DAO.commande;

import java.util.List;

import ma.fstt.DAO.IBaseDAO;
import ma.fstt.entities.Commande;

public interface ICommandeDAO extends IBaseDAO<Commande>{
	
	public List<Commande> ListByClient(int clientID) throws Exception ;
	
}
