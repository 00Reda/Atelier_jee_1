package ma.fstt.DAO.produit;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import javax.enterprise.context.ApplicationScoped;

import ma.fstt.DAO.BaseDAOImpl;
import ma.fstt.DAO.produit.IProduitDAO;
import ma.fstt.entities.Client;
import ma.fstt.entities.Produit;

@ApplicationScoped 
public class ProduitDAOImpl extends BaseDAOImpl implements IProduitDAO{

	public ProduitDAOImpl() throws Exception {
		super();
		// TODO Auto-generated constructor stub
	}

	
	private Produit getObjectFromDB() throws Exception{
		 
		 int _iD;
		 String _ref;
		 String _modele;
		 String _nom;
		 int _quantite;
		 Date _date_production;
		 
		 _iD=this.rs.getInt("ID");
		 _ref=this.rs.getString("ref");
		 _nom=this.rs.getString("nom");
		 _modele=this.rs.getString("modele");
		 _quantite=this.rs.getInt("quantite");
		 _date_production=this.format.parse(this.rs.getString("date_production"));
		 
		
		Produit p=new Produit(_iD, _ref, _nom, _modele, _date_production, _quantite, null);
		 return p;
	}
	
	@Override
	public Produit findByID(int ID) throws Exception {
		// TODO Auto-generated method stub
			String sql ="select * from produit where ID='"+ID+"';";
		
			this.rs=this.stm.executeQuery(sql);
			
			if (this.rs.next()) {
				
				 Produit c=this.getObjectFromDB();
				 
				 return c;
				
			}
			
			return null;
	}

	@Override
	public List<Produit> list() throws Exception {
		// TODO Auto-generated method stub
		      String sql ="select * from produit order by ID desc;";
				
				this.rs=this.stm.executeQuery(sql);
				ArrayList<Produit> list =new ArrayList<Produit>();
				while (this.rs.next()) {
					
					 Produit c=this.getObjectFromDB();
					 
					 list.add(c);
					
				}
				
				return list;
	}

	@Override
	public void add(Produit elm) throws Exception {

		String date=this.format.format(elm.get_date_production());
		String sql="INSERT INTO produit (ID, Ref, Nom, Modele, Date_production, Quantite) VALUES (NULL, '"+elm.get_ref()+"', '"+elm.get_nom()+"', '"+elm.get_modele()+"', '"+date+"', '"+elm.get_quantite()+"');";
	
		if(this.stm.executeUpdate(sql)!=1) throw new Exception("imposssible de cree votre produit !!");
	
	}

	@Override
	public void update(Produit elm) throws Exception {
		String date=this.format.format(elm.get_date_production());
		String sql="UPDATE produit SET Ref = '"+elm.get_ref()+"', Nom = '"+elm.get_nom()+"', Modele = '"+elm.get_modele()+"' , Date_production = '"+date+"', Quantite = "+elm.get_quantite()+" WHERE produit.ID = '"+elm.get_iD()+"';";
		if(this.stm.executeUpdate(sql)!=1) throw new Exception("imposssible de modifier votre produit !!");
	}

	@Override
	public void delete(int ID) throws Exception {

		String sql="DELETE FROM produit WHERE produit.ID = '"+ID+"' ;";
		if(this.stm.executeUpdate(sql)!=1) throw new Exception("imposssible de supprimer votre produit !!");
           
	}

}
