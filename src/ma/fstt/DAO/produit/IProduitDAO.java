package ma.fstt.DAO.produit;

import ma.fstt.DAO.IBaseDAO;
import ma.fstt.entities.Produit;

public interface IProduitDAO extends IBaseDAO<Produit>{

}
