package ma.fstt.DAO.client;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;
 
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import javax.enterprise.context.ApplicationScoped;

import ma.fstt.DAO.BaseDAOImpl;
import ma.fstt.entities.Client;

@ApplicationScoped 

public class ClientDAOImpl extends BaseDAOImpl implements IClientDAO{

	 
	
	public ClientDAOImpl() throws Exception {
		super();
		// TODO Auto-generated constructor stub
	}

	private Client getObjectFromDB() throws Exception{
		 int _iD;
		 String _email;
		 String _nom;
		 String _passwoed;
		 String _token;
		 String _tel;
		 java.util.Date _created_at;
		 Date _last_login;
		 
		 _iD=this.rs.getInt("ID");
		 _email=this.rs.getString("email");
		 _nom=this.rs.getString("nom");
		 _passwoed=this.rs.getString("password");
		 _token=this.rs.getString("token");
		 _tel=this.rs.getString("tel");
		 
		
		 _created_at=format.parse(rs.getString("created_at"));
		 _last_login=format.parse(rs.getString("last_login"));
		 
		 Client c=new Client(_iD, _email, _nom, _passwoed, _token, _tel, _created_at, _last_login, new Vector<>());
		 return c;
	}
	
	public Client findByID(int ID) throws Exception{

		String sql ="select * from client where ID='"+ID+"';";
		
		this.rs=this.stm.executeQuery(sql);
		
		if (this.rs.next()) {
			
			 Client c=this.getObjectFromDB();
			 
			 return c;
			
		}
		
		return null;
	}

	
	public List<Client> list() throws Exception {
		// TODO Auto-generated method stub
		String sql ="select * from client order by ID desc;";
		
		this.rs=this.stm.executeQuery(sql);
		ArrayList<Client> list =new ArrayList<Client>();
		while (this.rs.next()) {
			
			 Client c=this.getObjectFromDB();
			 
			 list.add(c);
			
		}
		
		return list;
	}

	 
	public void add(Client elm) throws Exception {
		// TODO Auto-generated method stub
		String created_at="";
		String last_login="";
		
		created_at=format.format(elm.get_created_at());
		last_login=format.format(elm.get_last_login());
		
		String sql ="INSERT INTO client ( ID , Email, Nom, Password, Token, Tel, Created_at, Last_login ) "
				+ "VALUES (NULL, '"+elm.get_email()+"', '"+elm.get_nom()+"', '"+elm.get_password()+"', '"+elm.get_token()+"', '"+elm.get_tel()+"', '"+created_at+"', '"+last_login+"');";
	
		if(this.stm.executeUpdate(sql)!=1) throw new Exception("imposssible de cree votre compte !!");
		
		
		
	}

	 
	public void update(Client elm) throws Exception {
		// TODO Auto-generated method stub
		String sql="UPDATE client SET Nom = '"+elm.get_nom()+"', Password = '"+elm.get_password()+"' , tel = '"+elm.get_tel()+"'  , email = '"+elm.get_email()+"' WHERE ID = '"+elm.get_iD()+"';";
	    
		if(this.stm.executeUpdate(sql)!=1) throw new Exception("imposssible de modifier votre compte !!");
	
	}

	 
	public void delete(int ID) throws Exception {

		String sql="DELETE FROM client WHERE client.ID = '"+ID+"';";
		if(this.stm.executeUpdate(sql)!=1) throw new Exception("imposssible de supprimer votre client !!");
		
	}
	
	

}
