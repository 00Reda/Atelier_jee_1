package ma.fstt.DAO.ligneCommande;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import javax.enterprise.context.ApplicationScoped;

import ma.fstt.DAO.BaseDAOImpl;
import ma.fstt.entities.Client;
import ma.fstt.entities.Commande;
import ma.fstt.entities.Ligne_commande;
import ma.fstt.entities.Produit;

@ApplicationScoped

public class LigneCommandeDAOImpl extends BaseDAOImpl implements ILigneCommandeDAO {

	
	public LigneCommandeDAOImpl() throws Exception {
		super();
		// TODO Auto-generated constructor stub
	}

	
	private Ligne_commande getObjectFromDB() throws Exception{
		
		 int _iD;
		 int _quantite;
		 Produit _produit=new Produit();
		 Commande _commande=new Commande();
				 
		 _iD=this.rs.getInt("ID");
		 _quantite=this.rs.getInt("quntite");
		 _produit.set_iD(this.rs.getInt("prduitID"));
		 _commande.set_iD(this.rs.getInt("commandeID"));
		
		 Ligne_commande c=new Ligne_commande(_iD, _quantite, _commande, _produit);
		
		 return c;   
	}
	
	
	@Override
	public Ligne_commande findByID(int ID) throws Exception {
	
		String sql ="select * from ligne_commande where ID='"+ID+"';";
		
		this.rs=this.stm.executeQuery(sql);
		
		if (this.rs.next()) {
			
			 Ligne_commande c=this.getObjectFromDB();
			 
			 return c;
			
		}
		return null;
	}

	@Override
	public List<Ligne_commande> list() throws Exception {
		// TODO Auto-generated method stub
				String sql ="select * from ligne_commande order by ID desc;";
				
				this.rs=this.stm.executeQuery(sql);
				ArrayList<Ligne_commande> list =new ArrayList<Ligne_commande>();
				while (this.rs.next()) {
					
					Ligne_commande c=this.getObjectFromDB();
					 
					 list.add(c);
					
				}
				
				return list;
	}
	
	public List<Ligne_commande> listByCommande(int id) throws Exception {
		// TODO Auto-generated method stub
				String sql ="select * from ligne_commande where commandeID= '"+id+"' order by ID desc;";
				
				this.rs=this.stm.executeQuery(sql);
				ArrayList<Ligne_commande> list =new ArrayList<Ligne_commande>();
				while (this.rs.next()) {
					
					Ligne_commande c=this.getObjectFromDB();
					 
					 list.add(c);
					
				}
				
				return list;
	}

	@Override
	public void add(Ligne_commande elm) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(Ligne_commande elm) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(int ID) throws Exception {
		// TODO Auto-generated method stub
		
	}

}
