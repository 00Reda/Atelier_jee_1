package ma.fstt.DAO.ligneCommande;

import java.util.List;

import ma.fstt.DAO.IBaseDAO;
import ma.fstt.entities.Ligne_commande;

public interface ILigneCommandeDAO extends IBaseDAO<Ligne_commande>{
	public List<Ligne_commande> listByCommande(int id) throws Exception;
}
