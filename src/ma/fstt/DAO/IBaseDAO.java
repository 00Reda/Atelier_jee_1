package ma.fstt.DAO;

import java.sql.ResultSet;
import java.util.List;

public interface IBaseDAO <T>{
	
	
	public  T findByID(int ID) throws Exception;
	public  List<T> list() throws Exception;
	public  void add(T elm) throws Exception;
	public  void update(T elm) throws Exception;
	public  void delete(int ID) throws Exception;

}
