package ma.fstt.DAO;

import java.util.List;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;

import com.mysql.jdbc.Driver;


public class BaseDAOImpl  {
	
	protected ResultSet rs=null;
	protected Statement stm=null;
	protected Connection cnx=null ;
	protected PreparedStatement pstm=null;
	protected SimpleDateFormat format=new SimpleDateFormat("YYYY-MM-dd");
	private String url="jdbc:mysql://localhost:3306/atelier_jee_1";
	private String user="root";
	private String password ="";
	
	
	
	public BaseDAOImpl() throws Exception{
		
		
		this.setDataBaseCnx();
		
	}
	
	private synchronized void setDataBaseCnx() throws Exception {
		
		if(this.cnx==null) {
			
			Class.forName("com.mysql.jdbc.Driver");
		    this.cnx=DriverManager.getConnection(url, user, password);
		    this.stm=this.cnx.createStatement();
			
		}
		
	}
	
	
	
	
	

}
