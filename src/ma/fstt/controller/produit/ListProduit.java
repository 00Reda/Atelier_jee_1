package ma.fstt.controller.produit;

import java.io.IOException;
import java.util.ArrayList;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ma.fstt.DAO.client.IClientDAO;
import ma.fstt.DAO.produit.IProduitDAO;
import ma.fstt.entities.Client;
import ma.fstt.entities.Produit;

/**
 * Servlet implementation class ListProduit
 */
@WebServlet("/Produit/list")
public class ListProduit extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	@Inject 
	private IProduitDAO model ;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ListProduit() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String id=request.getParameter("id");
		try {
			
			ArrayList<Produit> list = null;
			if(id==null)
			list=(ArrayList<Produit>)model.list();
			else {
				 Produit p =model.findByID(Integer.parseInt(id));
				 list=new ArrayList<Produit>();
			 	 list.add(p);
			}
			
			request.setAttribute("list", list);
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			request.setAttribute("error", e.getMessage());
		}
		
		this.getServletContext().getRequestDispatcher("/produit/list.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
