package ma.fstt.controller.produit;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import ma.fstt.DAO.produit.IProduitDAO;

/**
 * Servlet implementation class Delete
 */
@WebServlet("/Produit/delete")
public class Delete extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	@Inject 
	private IProduitDAO model ;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Delete() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
 
		String id=request.getParameter("id");
		Gson gson=new Gson();
		
		if(id==null) {
			
					request.getSession().setAttribute("error","aucun produit selectioner !!");
				    response.sendRedirect(getServletContext().getContextPath()+"/Produit/list");
		
		}else {
			
			try {
				
				   this.model.delete(Integer.parseInt(id));
				   request.getSession().setAttribute("msg","supprimé !!");
				   response.sendRedirect(getServletContext().getContextPath()+"/Produit/list");
				   
			} catch (Exception e) {
				
				   
				   request.getSession().setAttribute("error",e.getMessage());
				   response.sendRedirect(getServletContext().getContextPath()+"/Produit/list");
			}
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
