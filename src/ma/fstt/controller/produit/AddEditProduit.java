package ma.fstt.controller.produit;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ma.fstt.DAO.produit.IProduitDAO;
import ma.fstt.entities.Client;
import ma.fstt.entities.Produit;

/**
 * Servlet implementation class AddEditProduit
 */
@WebServlet("/Produit")
public class AddEditProduit extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private SimpleDateFormat format=new SimpleDateFormat("YYYY-MM-dd");
	
	@Inject 
	private IProduitDAO model ;   
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddEditProduit() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		   
		   String id=request.getParameter("id");
		   if(id==null) {
			   
			   request.setAttribute("produit", new Produit());
			   this.getServletContext().getRequestDispatcher("/produit/add.jsp").forward(request, response);
			   
		   }else {
			   
			   Produit c=null;
			   try {
				   
				 c=model.findByID(Integer.parseInt(id));
				 
				 if(c==null) {
					 
					   request.getSession().setAttribute("error", "produit n'exist pas !!");
					   response.sendRedirect("Produit/list");
					  
					   
				   }else {
					   
					     request.setAttribute("produit", c);
						 getServletContext().getRequestDispatcher("/produit/add.jsp").forward(request, response);
						 
				   }
				 
			   }catch (Exception e) {
				   
				 e.printStackTrace();
				 request.getSession().setAttribute("error", e.getMessage());
				 response.sendRedirect("Produit/list");
				 
			   }
			  
		   }
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		 String _iD;
		 String _ref;
		 String _modele;
		 String _nom;
		 String _quantite;
		 Date _date_production=new Date();
		 
		 _iD=request.getParameter("id");
		 _ref=request.getParameter("ref");
		 _nom=request.getParameter("nom");
		 _modele=request.getParameter("modele");
		 _quantite=request.getParameter("quantite");
		 try {
			_date_production=this.format.parse(request.getParameter("date_production"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
		 
		 StringBuffer err=new StringBuffer("");
		 
		 
		 if(_ref.equals("")) {
			 err.append("ref est obligatoire !<br>");
		 }
		 if(_modele.equals("")) {
			 err.append("modele est obligatoire !<br>");
		 }
		 if(_nom.equals("")) {
			 err.append("nom est obligatoire !<br>");
		 }
		 if(_quantite.equals("")) {
			 err.append("quantite est obligatoire !<br>");
		 }
		 
		
		 
		 if(err.length()>0) {
			 
			request.setAttribute("error", err.toString());
			if(_iD.equals("0")) {
				request.setAttribute("produit", new Client());
				
			}
			else {
				request.setAttribute("produit", new Produit( Integer.parseInt(_iD), _ref, _nom, _modele, _date_production, Integer.parseInt(_quantite), null));
			}
			
			this.getServletContext().getRequestDispatcher("/produit/add.jsp").forward(request, response);
			 
		 }else {
			   
					 
			 
			 if(_iD.equals("0")) {
				 
				 // add user
				 Produit c=new Produit( -1, _ref, _nom, _modele, _date_production, Integer.parseInt(_quantite), null);
				 
				try {
					 				 
					 this.model.add(c);
					 request.getSession().setAttribute("msg", " produit bien ajouté ! ");
					 response.sendRedirect("Produit/list");
					 
				} catch (Exception e) {
					
					request.getSession().setAttribute("error",e.getMessage());
					response.sendRedirect("Produit/list");
					
				}
				 
			 }else {
				 
				 // edit user 
				 Produit c=new Produit( Integer.parseInt(_iD), _ref, _nom, _modele, _date_production, Integer.parseInt(_quantite), null);
				
				 try {
	 				 
					 this.model.update(c);
					 request.getSession().setAttribute("msg", "produit bien Modifié !");
					 response.sendRedirect("Produit/list");
					 
				} catch (Exception e) {
					
					request.getSession().setAttribute("error",e.getMessage());
					response.sendRedirect("Produit/list");
					
				}
				 
			 }
			 
		 }
		 
		 
	}

}
