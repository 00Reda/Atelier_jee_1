package ma.fstt.controller.Client;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jdt.internal.compiler.ast.ArrayAllocationExpression;

import com.google.gson.Gson;

import ma.fstt.DAO.client.IClientDAO;
import ma.fstt.entities.Client;

/**
 * Servlet implementation class Client
 */
@WebServlet("/Client")
public class AddEditClient extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	private SimpleDateFormat format=new SimpleDateFormat("YYYY-MM-dd");
	
	private Gson gson=new Gson();
	
	@Inject 
	private IClientDAO model ;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddEditClient() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
	   String id=request.getParameter("id");
	   if(id==null) {
		   
		   request.setAttribute("client", new Client());
		   this.getServletContext().getRequestDispatcher("/client/add.jsp").forward(request, response);
		   
	   }else {
		   
		   Client c=null;
		   try {
			   
			 c=model.findByID(Integer.parseInt(id));
			 
			 if(c==null) {
				 
				   request.getSession().setAttribute("error", "Client n'exist pas !!");
				   response.sendRedirect("Client/list");
				  
				   
			   }else {
				   
				     request.setAttribute("client", c);
					 getServletContext().getRequestDispatcher("/client/add.jsp").forward(request, response);
					 
			   }
			 
		   }catch (Exception e) {
			   
			 e.printStackTrace();
			 request.getSession().setAttribute("error", e.getMessage());
			 response.sendRedirect("Client/list");
			 
		   }
		  
	   }
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		 String _iD;
		 String _email;
		 String _nom;
		 String _passwoed;
		 String _token;
		 String _tel;
		 Date _created_at=new Date();
		 Date _last_login=new Date();
		 
		 _iD=request.getParameter("id");
		 _email=request.getParameter("email");
		 _nom=request.getParameter("nom");
		 _passwoed=request.getParameter("password");
		 _token="";
		 _tel=request.getParameter("tel");
		 
		 
		StringBuffer err=new StringBuffer("");
		 
		 
		 if(_email.equals("")) {
			 err.append("email est obligatoire !<br>");
		 }
		 if(_tel.equals("")) {
			 err.append("tel est obligatoire !<br>");
		 }
		 if(_nom.equals("")) {
			 err.append("nom est obligatoire !<br>");
		 }
		 if(_passwoed.equals("")) {
			 err.append("password est obligatoire !<br>");
		 }
		 
		
		 
		 if(err.length()>0) {
			 
			request.setAttribute("error", err.toString());
			if(_iD.equals("0")) {
				request.setAttribute("client", new Client());
				
			}
			else {
				request.setAttribute("client", new Client(Integer.parseInt(_iD), _email, _nom, _passwoed, _token, _tel, _created_at, _last_login, new Vector<>()));
			}
			
			this.getServletContext().getRequestDispatcher("/client/add.jsp").forward(request, response);
			 
		 }else {
			   
					_created_at=new Date();
					_last_login=new Date();
			 
			 if(_iD.equals("0")) {
				 
				 // add user
				 Client c=new Client(-1, _email, _nom, _passwoed, _token, _tel, _created_at, _last_login, new Vector<>());
				 
				try {
					 				 
					 this.model.add(c);
					 request.getSession().setAttribute("msg", " Client bien ajouté ! ");
					 response.sendRedirect("Client/list");
					 
				} catch (Exception e) {
					
					request.getSession().setAttribute("error",e.getMessage());
					response.sendRedirect("Client/list");
					
				}
				 
			 }else {
				 
				 // edit user 
				 Client c=new Client(Integer.parseInt(_iD), _email, _nom, _passwoed, _token, _tel, _created_at, _last_login, new Vector<>());
				
				 try {
	 				 
					 this.model.update(c);
					 request.getSession().setAttribute("msg", "Client bien Modifié !");
					 response.sendRedirect("Client/list");
					 
				} catch (Exception e) {
					
					request.getSession().setAttribute("error",e.getMessage());
					response.sendRedirect("Client/list");
					
				}
				 
			 }
			 
		 }
		 
		 
		 
		
		 
		
	}

}
