package ma.fstt.controller.Client;

import java.io.IOException;
import java.util.ArrayList;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jdt.internal.compiler.ast.ArrayAllocationExpression;

import ma.fstt.DAO.IBaseDAO;
import ma.fstt.DAO.client.IClientDAO;
import ma.fstt.entities.Client;
import ma.fstt.entities.Produit;

/**
 * Servlet implementation class ListClient
 */
@WebServlet("/Client/list")
public class ListClient extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	
	@Inject 
	private IClientDAO model ;
       
    public ListClient() {
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String id=request.getParameter("id");
		try {
			
			ArrayList<Client> list = null;
			if(id==null)
			list=(ArrayList<Client>)model.list();
			else {
				 Client p =model.findByID(Integer.parseInt(id));
				 list=new ArrayList<Client>();
			 	 list.add(p);
			}
			
			request.setAttribute("list", list);
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			request.setAttribute("error", e.getMessage());
		}
		
		this.getServletContext().getRequestDispatcher("/client/list.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
