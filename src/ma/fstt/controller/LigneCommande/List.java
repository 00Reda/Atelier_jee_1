package ma.fstt.controller.LigneCommande;

import java.io.IOException;
import java.util.ArrayList;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ma.fstt.DAO.commande.ICommandeDAO;
import ma.fstt.DAO.ligneCommande.ILigneCommandeDAO;
import ma.fstt.entities.Commande;
import ma.fstt.entities.Ligne_commande;

/**
 * Servlet implementation class List
 */
@WebServlet(name = "ListLigneCommande", urlPatterns = { "/ligneCommande/list" })
public class List extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	@Inject 
	private ILigneCommandeDAO LigneCommandeMedel;
	@Inject 
	private ICommandeDAO CommandeMedel;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public List() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		String id=request.getParameter("id");
		
		try {
			
			ArrayList<Ligne_commande> list = null;
			Commande c=CommandeMedel.findByID(Integer.parseInt(id));
			if(id==null)
			list=(ArrayList<Ligne_commande>)LigneCommandeMedel.list();
			else list=(ArrayList<Ligne_commande>)LigneCommandeMedel.listByCommande(Integer.parseInt(id));
			
			request.setAttribute("list", list);
			request.setAttribute("commande", c);
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			request.setAttribute("error", e.getMessage());
		}
		
		this.getServletContext().getRequestDispatcher("/ligneCommande/list.jsp").forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
