package ma.fstt.controller.commande;

import java.io.IOException;
import java.util.ArrayList;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ma.fstt.DAO.commande.ICommandeDAO;
import ma.fstt.entities.Commande;
import ma.fstt.entities.Produit;

/**
 * Servlet implementation class List
 */
@WebServlet("/Commande/list")
public class List extends HttpServlet {
	private static final long serialVersionUID = 1L;
    @Inject
    private ICommandeDAO CommandeMedel;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public List() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String id=request.getParameter("id");
		
		try {
			
			ArrayList<Commande> list = null;
			if(id==null)
			list=(ArrayList<Commande>)CommandeMedel.list();
			else list=(ArrayList<Commande>)CommandeMedel.ListByClient(Integer.parseInt(id));
			
			request.setAttribute("list", list);
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			request.setAttribute("error", e.getMessage());
		}
		
		this.getServletContext().getRequestDispatcher("/commande/list.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
